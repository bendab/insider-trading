#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <vector>
#include <parameters.h>
#include <gameoptionunderIF.h>
#include <gameoptionunderIG.h>
#include <LSM_IF.h>
#include <LSM_IG.h>
#include <LSM_G_Fversion.h>
#include <plot.h>

using namespace std;

inline double Ct (double time);
inline double p_in (int i, vector<double> x, double wt, double t, double ti, double delta);
inline bool equal(double a, double b);


int main()
{
    double value;
    vector<double> valueLSMF(n_ld+1), valueLSMG(n_ld+1), valueLSMG2(n_ld+1);

    double T_IG, T_IF;
    T_IF = T_IG = M;

    cout << "s0 = " << s0 << ", K = " << K << ", sigma = " << sigma << ", mu = " << mu << ", r = " << r
         << ", Maturity = " << M << ", penalty = " << penalty << endl;
    cout << "x0 = " << x0 << ", epsilon = " << epsilon_ext<< ", m = " << n_ld << endl;//", penalization = " << penalization << endl;
    cout << "m = " << n_ld << ", N = " << N << ", Order = " << Order << ", T_IG = " << T_IF << endl<< endl;

    value = monte_carlo_call_price(N, s0, K, r, sigma, T_IG);
    cout << "European value (!) = " << value << endl << endl;


    //plotKvariable (60.0f, 140.0f, 5);

    plot(M, n_ld);

//    valueLSMF = LSMinIF(T_IF);
//    plot2d(valueLSMF, "pF", "F");
//    //cout << "LSM value in IF = " << valueLSMF << endl<< endl;

//    valueLSMG = LSMinIG_epsilonRV_F_version(T_IG);
//    plot2d(valueLSMG, "pGF", "G with payoff in F");

    valueLSMG2 = LSMinIG(T_IG);
    plot2d(valueLSMG2, "pG", "G");

    ////////////////////////
    ////////////////////////
    /////Stopping Time//////
    ////////////////////////
    ////////////////////////
    StoppingRegion(M,100,1000,K,K+100,0.0);

    return 0;
}
