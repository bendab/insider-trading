#ifndef LSM_G_FVERSION_H
#define LSM_G_FVERSION_H
#include <iostream>
#include <vector>
#include <cmath>
#include <LSM.h>
#include <algorithm>
#include <vector>
#include <parameters.h>

using namespace std;
vector<double> LSMinIG_epsilonRV_F_version(double TG);
int simulateGBMPath_Fversion(double dt, vector<double> * Payoff,
                    vector<vector<double>> * S,
                    vector<vector<double>> * p_inPositive, vector<vector<double> > *p);

vector<double> LSMinIG_epsilonRV_F_version(double TG)
{
    double dt = (1.0/((double) n_ld)) * TG;
    vector<vector<double>> S, p_inPositive, p;/*, ITMpath (N);*/
    vector<double> Payoff, price(n_ld+1);//payoff
    double CE;//continuation region
    double Discount = exp(-r*dt);//Discount factor

    //We simulate the paths
    int nb_itm_path = 0.0;
    nb_itm_path = simulateGBMPath_Fversion(dt, &Payoff, &S, &p_inPositive, &p);
    vector<double> tau (nb_itm_path), gamma (nb_itm_path);//optimal stopping time

    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [n_ld] += 1.0/(2.0*N) * Discount * Payoff[i];
        tau [i] = gamma [i] = TG;
    }

    Eigen::VectorXd beta(Order);
    for(int t = n_ld-1; t > 0; t--)//time
    {
        beta = beta_Laguerre_eigen_version(t, nb_itm_path, Order, Discount, &Payoff, &S, &p);

        for(int i = 0; i < nb_itm_path; i++)//i = N simulations
        {
            CE = 0.0;
            double Weight = exp(-0.5*S[i][t]);
            for (int j = 0; j <= Order; j++)//j = laguerre polynimial
            {
                CE += beta[j]*Discount*Weight*boost::math::laguerre(j, S[i][t]);
            }

            double tmp_option = option (S[i][t], K);
            double extra = p_inPositive[i][t];
            //min(max(Option, E(...|F_t)),Option+Penalty)//Snell envelop
            //we are upon the Upper barrier of the CR
            if (tmp_option*extra < CE)//CE = max(L,CE)
            {
                if (CE < (tmp_option + penalty)*extra)//min(U,CE)=CE
                {
                    Payoff [i] = Discount*Payoff [i]*extra/**(p[i][t])*/;
                }
                //we are below the Lower barrier of the CR
                else //min(U,CE)=U
                {
                    Payoff [i] = (tmp_option + penalty)*extra;
                    gamma [i] = t*dt;
                }
            }
            else if (CE <= tmp_option*extra) //min(U,max(L,CE))=L
            {
                Payoff [i] = tmp_option*extra;//==g(S^i_t)
                tau [i] = t*dt;
            }

            price[t] += 1.0/(2.0*N) * Payoff[i];
        }
    }

    double tau_opt = 0.0, sigma_opt = 0.0;
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [0] += 1.0/(2.0*N) * Discount * Payoff[i];
        tau_opt += 1.0/(2.0*N) * tau [i];
        sigma_opt += 1.0/(2.0*N) * gamma [i];
    }

    cout << "disp('Average F version of the payoff tau G = " << tau_opt << ", sigma G = " << sigma_opt << "')" << endl;

    return price;
}


int simulateGBMPath_Fversion(double dt, vector<double> * Payoff,
                    vector<vector<double>> * S,
                    vector<vector<double>> * p_inPositive, vector<vector<double>> * p)
{
    vector<double> Y(n_ld+1), epsilon;
    vector<vector<double>> * W, * X;
    double dW, dX;
    W = simulateBM (dt);
    X = simulateExtraInformation(dt, &epsilon);
    int lenghtdW = (*W).size();
    int nb_itm_paths = 0.0;

    Y[0] = s0;
    for (int i = 0; i < lenghtdW; i++)//N == simultations
    {
        bool bool_itm_path = false;
        double tmpProd = 1.0;
        vector<double> tmp_IP_in(n_ld+1), tmp_Vector_Pin_indicator(n_ld+1);
        double epsilon_square = epsilon[i]*epsilon[i];
        tmp_IP_in[0] = normal_pdf (x0-w0, 0.0, sqrt(T*(1.0+epsilon_square)));
        if (tmp_IP_in[0] > 0.001)
            tmp_Vector_Pin_indicator[0] = 1.0;
        else
        {
            tmp_Vector_Pin_indicator[0] = 0.0;
            tmp_IP_in[0] = 0.0;
        }
        if (option(s0, K) > 0.01)
        {
            bool_itm_path = true;
            nb_itm_paths++;
        }
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            dW = (*W)[i][t] - (*W)[i][t-1];
            Y [t] = Y [t-1]*exp( (r-0.5*sigma_square)*dt + sigma*dW );
            if(!bool_itm_path && option(Y[t], K) > 0.001)
            {
                bool_itm_path = true;
                nb_itm_paths++;
            }

            //p building
            dX = (*X)[i][t]-(*X)[i][t-1];
            tmpProd *= 1.0/sqrt(dt) * normal_pdf ( -dX/dt, 0.0, 1.0 );
            tmp_IP_in[t] = normal_pdf ( ((*X)[i][t]-(*W)[i][t])/sqrt((T-t*dt)*(1.0+epsilon_square)), 0.0, 1.0 ) * tmpProd;
//            for (int j = 0; j <= t-1; j++)
//            {
//                dX = (*X)[i][j+1]-(*X)[i][j];
//                tmp_IP_in[t] *= 1.0/sqrt(dt) * normal_pdf ( -dX/dt, 0.0, 1.0 );
//            }

            if (tmp_IP_in[t] > 0.01 || t*dt > T)//or extra info T happen before the option maturity i.e. M>T
                tmp_Vector_Pin_indicator[t] = 1.0;
            else
            {
                tmp_Vector_Pin_indicator[t] = 0.0;
                tmp_IP_in[t] = 0.0;
            }
            //p building
        }

        if(bool_itm_path)//path in the money
        {
            (*S).push_back(Y);
            (*p_inPositive).push_back(tmp_Vector_Pin_indicator);
            (*p).push_back(tmp_IP_in);
            double extra = tmp_Vector_Pin_indicator[n_ld]/*/tmp_IP_in[m]*/;
            (*Payoff).push_back(option (Y[n_ld], K)*extra);//==L_T, exp(-r*(T-T))=1.0
        }
    }

    //free (*dW)[1];
    return nb_itm_paths;
}

#endif // LSM_G_FVERSION_H
