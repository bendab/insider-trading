#ifndef LSM_IG_H
#define LSM_IG_H
#include <iostream>
#include <vector>
#include <cmath>
#include <LSM.h>
#include <algorithm>
#include <parameters.h>

using namespace std;

vector<double> LSMinIG(double TG, bool verbose);
vector<double> LSMinIGOld(double TG);
vector<double> LSMinIG_epsilonRVOld(double TG);
double LSMinIG_ForStoppingRegion(double TG, vector<vector<double>> * W, vector<vector<double>> * X, bool verbose);

int simulateGBMPathInG_ForStoppingRegion(double dt, vector<double> * Payoff, vector<vector<double>> * S,
                       vector<vector<double>> * W, vector<vector<double>> * X);
int simulateGBMPathInG(double dt, vector<double> * Payoff, vector<vector<double>> * S);

vector<double> LSMinIG(double TG, bool verbose = true)
{
    double dt = (1.0/((double) n_ld)) * TG;
    vector<vector<double>> S;
    vector<double> price(n_ld+1);
    vector<double> Payoff;
    vector<double> dW(n_ld+1);
    double tmp_option;
    double Discount = exp(-r*dt);//Discount factor

    //We simulate the paths
    int nb_itm_path = 0.0;
    nb_itm_path = simulateGBMPathInG(dt, &Payoff, &S);

    vector<double> tau (nb_itm_path), gamma (nb_itm_path);//optimal stopping time

    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [n_ld] += 1.0/(2*(N)) * Payoff[i];
        tau [i] = gamma [i] = TG;
    }

    Eigen::VectorXd beta(Order);
    for(int t = n_ld-1; t > 0; t--)//time
    {
        beta = beta_Laguerre_eigen_version(t, nb_itm_path, Order, Discount, &Payoff, &S);

        for(int i = 0; i < nb_itm_path; i++)//i = m simulations
        {
            tmp_option = option (S[i][t], K);
            double CE = 0.0; //CE = Conditional Expectation
            double Weight = exp(-0.5*S[i][t]);
            for (int j = 0; j <= Order; j++)//j = laguerre polynimial
            {
                CE += beta[j]*Discount*Weight*boost::math::laguerre(j, S[i][t]);
            }

            //min(max(Option, E(...|F_t)),Option+Penalty)//Snell envelop
            //we are upon the Upper barrier of the CR
            if (tmp_option <= CE)//CE = max(L,CE)
            {
                if (CE < tmp_option + penalty)//min(U,CE)=CE
                {
                    Payoff [i] = Discount*Payoff [i];
                }
                //we are below the Lower barrier of the CR
                else //min(U,CE)=U
                {
                    Payoff [i] = tmp_option + penalty;
                    gamma [i] = t*dt;
                }
            }
            else if (CE < tmp_option) //min(U,max(L,CE))=L
            {
                Payoff [i] = tmp_option;//==g(S^i_t)
                tau [i] = t*dt;
            }

            price [t] += (1.0/(2*(N))) * Payoff[i];
        }
    }

    double tau_opt = 0.0, sigma_opt = 0.0;
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [0] += (1.0/(2*N)) * Discount * Payoff[i];
        tau_opt += (1.0/(2*N)) * tau [i];
        sigma_opt += (1.0/(2*N)) * gamma [i];
    }

    //    cout << "Price = " << price [0] << endl;
    if (verbose)
        cout << "disp('Average tau G = " << tau_opt << ", sigma G = " << sigma_opt << "')"  << endl;
    return price;
}



////Version for the Stopping region
/// I just simulate the sample paths once
double LSMinIG_ForStoppingRegion(double TG, vector<vector<double>> * W, vector<vector<double>> * X, bool verbose = true)
{
    double dt = (1.0/((double) n_ld)) * TG;
    vector<vector<double>> S;
    vector<double> price(n_ld+1);
    vector<double> Payoff;
    vector<double> dW(n_ld+1);
    double tmp_option;
    double Discount = exp(-r*dt);//Discount factor

    //We simulate the paths
    int nb_itm_path = 0.0;
    nb_itm_path = simulateGBMPathInG_ForStoppingRegion(dt, &Payoff, &S, W, X);

    vector<double> tau (nb_itm_path), gamma (nb_itm_path);//optimal stopping time

    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [n_ld] += 1.0/(2*(N)) * Payoff[i];
        tau [i] = gamma [i] = TG;
    }

    Eigen::VectorXd beta(Order);
    for(int t = n_ld-1; t > 0; t--)//time
    {
        beta = beta_Laguerre_eigen_version(t, nb_itm_path, Order, Discount, &Payoff, &S);

        for(int i = 0; i < nb_itm_path; i++)//i = m simulations
        {
            tmp_option = option (S[i][t], K);
            double CE = 0.0; //CE = Conditional Expectation
            double Weight = exp(-0.5*S[i][t]);
            for (int j = 0; j <= Order; j++)//j = laguerre polynimial
            {
                CE += beta[j]*Discount*Weight*boost::math::laguerre(j, S[i][t]);
            }

            //min(max(Option, E(...|F_t)),Option+Penalty)//Snell envelop
            //we are upon the Upper barrier of the CR
            if (tmp_option <= CE)//CE = max(L,CE)
            {
                if (CE < tmp_option + penalty)//min(U,CE)=CE
                {
                    Payoff [i] = Discount*Payoff [i];
                }
                //we are below the Lower barrier of the CR
                else //min(U,CE)=U
                {
                    Payoff [i] = tmp_option + penalty;
                    gamma [i] = t*dt;
                }
            }
            else if (CE < tmp_option) //min(U,max(L,CE))=L
            {
                Payoff [i] = tmp_option;//==g(S^i_t)
                tau [i] = t*dt;
            }

            price [t] += (1.0/(2*(N))) * Payoff[i];
        }
    }

    double tau_opt = 0.0, sigma_opt = 0.0;
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [0] += (1.0/(2*N)) * Discount * Payoff[i];
        tau_opt += (1.0/(2*N)) * tau [i];
        sigma_opt += (1.0/(2*N)) * gamma [i];
    }

    //    cout << "Price = " << price [0] << endl;
    if (verbose)
        cout << "disp('Average tau G = " << tau_opt << ", sigma G = " << sigma_opt << "')"  << endl;
    return price[0];
}

//GBM paths in G
int simulateGBMPathInG(double dt, vector<double> * Payoff, vector<vector<double>> * S)
{
    vector<double> Y(n_ld+1), A(n_ld+1), epsilon;
    vector<vector<double>> * W = simulateBM (dt);
    vector<vector<double>> * X = simulateExtraInformation(dt, &epsilon);
    double tmp_sigma, tmp_sigma_square;
    double dW, Ct_tmp;
    int lenghtdW = (*W).size();
    int nb_itm_paths = 0.0;

    Y[0] = s0;
    for (int i = 0; i < lenghtdW; i++)//N == simultations
    {
        bool bool_itm_path = false;
        if (option(s0, K) > 0)
        {
            bool_itm_path = true;
            nb_itm_paths++;
        }
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            Ct_tmp = Ct(t*dt);
            dW = (*W)[i][t]-(*W)[i][t-1];
            //Jump vol
            tmp_sigma = volatility((*X)[i][t-1]);
            tmp_sigma_square = tmp_sigma*tmp_sigma;
            //Jump vol
            Y [t] = Y [t-1]*exp( (r - 0.5f*tmp_sigma_square + tmp_sigma*Ct_tmp*((*W)[i][t-1]-(*X)[i][t-1]))*dt + tmp_sigma*dW );
            if(!bool_itm_path && option(Y[t], K) > 0.001)
            {
                bool_itm_path = true;
                nb_itm_paths++;
            }
        }

        if(bool_itm_path)//path in the money
        {
            (*S).push_back(Y);
            (*Payoff).push_back(option (Y[n_ld], K));//==L_T, exp(-r*(T-T))=1.0
        }
    }

    delete W;
    delete X;
    return nb_itm_paths;
}

/////Version for the Stopping Region
int simulateGBMPathInG_ForStoppingRegion(double dt, vector<double> * Payoff, vector<vector<double>> * S,
                       vector<vector<double>> * W, vector<vector<double>> * X)
{
    vector<double> Y(n_ld+1), A(n_ld+1), epsilon;
    double tmp_sigma, tmp_sigma_square;
    double dW, Ct_tmp;
    int lenghtdW = (*W).size();
    int nb_itm_paths = 0.0;

    Y[0] = s0;
    for (int i = 0; i < lenghtdW; i++)//N == simultations
    {
        bool bool_itm_path = false;
        if (option(s0, K) > 0)
        {
            bool_itm_path = true;
            nb_itm_paths++;
        }
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            Ct_tmp = Ct(t*dt);
            dW = (*W)[i][t]-(*W)[i][t-1];
            //Jump vol
            tmp_sigma = volatility((*X)[i][t-1]);
            tmp_sigma_square = tmp_sigma*tmp_sigma;
            //Jump vol
            Y [t] = Y [t-1]*exp( (r - 0.5f*tmp_sigma_square + tmp_sigma*Ct_tmp*((*W)[i][t-1]-(*X)[i][t-1]))*dt + tmp_sigma*dW );
            if(!bool_itm_path && option(Y[t], K) > 0.001)
            {
                bool_itm_path = true;
                nb_itm_paths++;
            }
        }

        if(bool_itm_path)//path in the money
        {
            (*S).push_back(Y);
            (*Payoff).push_back(option (Y[n_ld], K));//==L_T, exp(-r*(T-T))=1.0
        }
    }

    return nb_itm_paths;
}

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////


vector<double> LSMinIGOld(double TG)
{
    double delta = (1.0/n_ld) * TG;
    vector<vector<double>> S(N), X(N), W(N), p_inPositive(N);/*, ITMpath (N);*/
    vector<double> P (N), price(n_ld+1);//payoff
    vector<double> C_down (N), C_up (N);//continuation region
    vector<double> tau (N), gamma (N);//optimal stopping time
    double tmp_epsilon = 0.0f;
    double tmp_sigma, tmp_sigma_square;

    double sup_tau = 0.0f, inf_gamma = 0.0f, sup_y = 0.0f, inf_y = 0.0f;
    double tmp_pi0, tmp_IP_in, dW;
    double beta_down_j, beta_up_j;

    //We simulate the paths
    tmp_pi0 = normal_pdf( (x0-w0)/sqrt(T*(1.0f+epsilon_square_ext)), 0.0f, 1.0f );
    for (int i = 0; i < N; i++)//N == simultations
    {
        S[i] = vector <double> (n_ld+1);
        X[i] = vector <double> (n_ld+1);
        W[i] = vector <double> (n_ld+1);
        p_inPositive[i] = vector <double> (n_ld+1);
        tau[i] = gamma [i] = TG;
        S[i][0] = s0, X[i][0] = x0; W[i][0] = w0;
        if (tmp_pi0 > 0)
            p_inPositive[i][0] = 1.0f;
        else
            p_inPositive[i][0] = 0.0f;
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            W[i][t] = W[i][t-1] + sqrt(delta)*gaussian_box_muller();
            X [i][t] = X [i][t-1] + epsilon_ext*sqrt(delta)*gaussian_box_muller();
            dW = W[i][t] - W[i][t-1];
            tmp_sigma = volatility(X [i][t]);
            tmp_sigma_square = tmp_sigma*tmp_sigma;
            S [i][t] = S [i][t-1]*exp( (r-0.5f*sigma_square)*delta + sigma*dW);

            tmp_IP_in = normal_pdf( (X[i][t]-W[i][t])/(sqrt((T-delta*t)*(1.0f+epsilon_square_ext))), 0.0f, 1.0f);
            //tmp_IP_in = normal_pdf (X[i][t]-W[i][t], 0.0f, sqrt((T-delta*t)*(1.0f+epsilon_square)));
            for (int j = 0; j <= t-1; j++)
            {
                tmp_IP_in *= (1.0f/sqrt(delta)) * normal_pdf( (-(X[i][j+1]-X[i][j]))/delta, 0.0f, 1.0f);
                //tmp_IP_in *= normal_pdf ( -(X[i][j+1]-X[i][j]), 0.0f, sqrt(delta));
            }

            if (tmp_IP_in > 0.0001f)
                p_inPositive[i][t] = 1.0f;
            else
                p_inPositive[i][t] = 0.0f;
        }
        P [i] = option (S[i][n_ld], K)*p_inPositive[i][n_ld];//==L_TG
    }

    for(int i = 0; i < N; i++)//i = m simulations
    {
        price [n_ld] += 1.0f/N * exp(-r*delta) * P[i];
        //        sup_y = inf_y = price;
        //        sup_tau = inf_gamma = m*delta;
    }

    for(int t = n_ld-1; t > 0; t--)//time
    {
        for(int i = 0; i < N; i++)//i = N simulations
        {
            C_down [i] = C_up [i] = 0.0f;
            for (int j = 0; j <= 3; j++)//j = laguerre polynimial
            {
                beta_down_j = 0.0;//regressionCoefficient (S[i][t], exp(-r*delta)* P [i], j);
                C_down [i] += 0.0;//beta_down_j*Laguerre_Polynomial(S[i][t], j);

                beta_up_j = 0.0;//regressionCoefficient (S[i][t], exp(-r*delta)* (P[i]+penalty), j);
                C_up [i] += 0.0;//beta_up_j*Laguerre_Polynomial(S[i][t], j);
            }

            double tmp_option = option (S[i][t], K)*p_inPositive[i][t];
            //we are below the Lower barrier of the CR
            if (tmp_option <= C_down[i] /*|| tmp_option <= 0.0f*/)
            {
                P [i] = exp(-r*delta)*P [i];
                tau [i] = t*delta;
            }
            //we are upon the Upper barrier of the CR
            else if (tmp_option >= C_up[i])
            {
                P [i] = exp(-r*delta)*(P [i] + penalty);
                gamma [i] = t*delta;
            }
            //we are inside the Continuation Region = CR
            //            else
            //                P [i] = tmp_option;
        }

        for(int i = 0; i < N; i++)//i = m simulations
        {
            price[t] += 1.0f/N * exp(-r*delta) * P[i];
            //            if (price >= sup_y)
            //            {
            //                sup_y = price;
            //                sup_tau = t*delta;
            //            }
            //            if (price <= inf_y)
            //            {
            //                inf_y = price;
            //                inf_gamma = t*delta;
            //            }
        }
    }

    double tau_opt = 0.0f, sigma_opt = 0.0f;
    for(int i = 0; i < N; i++)//i = m simulations
    {
        price [0] += 1.0f/N * exp(-r*delta) * P[i];
        //        if (price >= sup_y)
        //        {
        //            sup_y = price;
        //            sup_tau = 0.0f*delta;
        //        }
        //        if (price <= inf_y)
        //        {
        //            inf_y = price;
        //            inf_gamma = 0.0f*delta;
        //        }
        tau_opt += 1.0f/N * tau [i];
        sigma_opt += 1.0f/N * gamma [i];
    }

    cout << "Average tau G = " << tau_opt << ", sigma G = " << sigma_opt << endl;
    //cout << "sup tau G = " << sup_tau << ", inf sigma G = " << inf_gamma << endl;
    return price;
}



vector<double> LSMinIG_epsilonRVOld(double TG)
{
    double delta = (1.0/n_ld) * TG;
    vector<vector<double>> S(N), X(N), W(N), p_inPositive(N);/*, ITMpath (N);*/
    vector<double> P (N), price(n_ld+1);//payoff
    vector<double> C_down (N), C_up (N);//continuation region
    vector<double> tau (N), gamma (N);//optimal stopping time
    double tmp_epsilon = 0.0f;
    double tmp_sigma, tmp_sigma_square;

    double tmp_pi0, tmp_IP_in, dW;
    double beta_down_j, beta_up_j;

    //We simulate the paths
    for (int i = 0; i < N; i++)//N == simultations
    {
        S[i] = vector <double> (n_ld+1);
        X[i] = vector <double> (n_ld+1);
        W[i] = vector <double> (n_ld+1);
        p_inPositive[i] = vector <double> (n_ld+1);
        tau[i] = gamma [i] = TG;
        S[i][0] = s0, X[i][0] = x0; W[i][0] = w0;

        tmp_epsilon = gaussian_box_muller();
        tmp_pi0 = normal_pdf( (x0-w0)/sqrt(T*(1.0f+tmp_epsilon*tmp_epsilon)), 0.0f, 1.0f );
        if (tmp_pi0 > 0)
            p_inPositive[i][0] = 1.0f;
        else
            p_inPositive[i][0] = 0.0f;
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            W[i][t] = W[i][t-1] + sqrt(delta)*gaussian_box_muller();
            X [i][t] = X [i][t-1] + tmp_epsilon*sqrt(delta)*gaussian_box_muller();
            dW = W[i][t] - W[i][t-1];
            tmp_sigma = volatility(X [i][t]);
            tmp_sigma_square = tmp_sigma*tmp_sigma;
            S [i][t] = S [i][t-1]*exp( (r-0.5f*tmp_sigma_square)*delta + tmp_sigma*dW);

            //tmp_IP_in = normal_pdf( (X[i][t]-W[i][t])/(sqrt((T-delta*t)*(1.0f+tmp_epsilon*tmp_epsilon))), 0.0f, 1.0f);
            tmp_IP_in = normal_pdf (X[i][t]-W[i][t], 0.0f, sqrt((T-delta*t)*(1.0f+epsilon_square_ext)));
            for (int j = 0; j <= t-1; j++)
            {
                //tmp_IP_in *= (1.0f/sqrt(delta)) * normal_pdf( (-(X[i][j+1]-X[i][j]))/delta, 0.0f, 1.0f);
                tmp_IP_in *= normal_pdf ( -(X[i][j+1]-X[i][j]), 0.0f, sqrt(delta));
            }

            if (tmp_IP_in > 0.0001f)
                p_inPositive[i][t] = 1.0f;
            else
                p_inPositive[i][t] = 0.0f;
        }
        P [i] = option (S[i][n_ld], K)*p_inPositive[i][n_ld];//==L_TG
    }

    for(int i = 0; i < N; i++)//i = m simulations
    {
        price [n_ld] += 1.0f/N * exp(-r*delta) * P[i];
    }

    for(int t = n_ld-1; t > 0; t--)//time
    {
        for(int i = 0; i < N; i++)//i = N simulations
        {
            C_down [i] = C_up [i] = 0.0f;
            for (int j = 0; j <= 3; j++)//j = laguerre polynimial
            {
                beta_down_j = 0.0;//regressionCoefficient (S[i][t], exp(-r*delta)* P [i], j);
                C_down [i] += 0.0;//beta_down_j*Laguerre_Polynomial(S[i][t], j);

                beta_up_j = 0.0;//regressionCoefficient (S[i][t], exp(-r*delta)* (P[i]+penalty), j);
                C_up [i] += 0.0;//beta_up_j*Laguerre_Polynomial(S[i][t], j);
            }

            double tmp_option = option (S[i][t], K)*p_inPositive[i][t];
            //we are below the Lower barrier of the CR
            if (tmp_option <= C_down[i] /*|| tmp_option <= 0.0f*/)
            {
                P [i] = exp(-r*delta)*P [i];
                tau [i] = t*delta;
            }
            //we are upon the Upper barrier of the CR
            else if (tmp_option >= C_up[i])
            {
                P [i] = exp(-r*delta)*(P [i] + penalty);
                gamma [i] = t*delta;
            }
            //we are inside the Continuation Region = CR
            //            else
            //                P [i] = tmp_option;
        }

        for(int i = 0; i < N; i++)//i = m simulations
        {
            price[t] += 1.0f/N * exp(-r*delta) * P[i];
        }
    }

    double tau_opt = 0.0f, sigma_opt = 0.0f;
    for(int i = 0; i < N; i++)//i = m simulations
    {
        price [0] += 1.0f/N * exp(-r*delta) * P[i];
        tau_opt += 1.0f/N * tau [i];
        sigma_opt += 1.0f/N * gamma [i];
    }
    cout << "Average tau G = " << tau_opt << ", sigma G = " << sigma_opt << endl;

    return price;
}

#endif // LSM_IG_H
