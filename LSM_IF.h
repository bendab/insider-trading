#ifndef LSM_IF_H
#define LSM_IF_H
#include <iostream>
#include <cmath>
#include <LSM.h>
#include <algorithm>
#include <parameters.h>
#include <time.h>
#include <random>

///////BOOST LIBRARIES//////
//#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/numeric/ublas/vector.hpp>
//#include <boost/numeric/ublas/io.hpp>
//#include <boost/numeric/ublas/vector_proxy.hpp>
//#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/numeric/ublas/triangular.hpp>
//#include <boost/numeric/ublas/lu.hpp>
#include <boost/math/special_functions/laguerre.hpp>
#include <invertMatrix.h>
#include <eigen/Dense>
//using namespace boost::numeric::ublas;
namespace ublas = boost::numeric::ublas;
///////BOOST LIBRARIES//////
#include <vector>

using namespace std;

vector<double> LSMinIF(double TF);

vector<vector<double>> * simulateFBrownian(double dt);
double simulateGBMPath(double dt, vector<double> * Payoff, vector<vector<double>> * S);

vector<double> LSMinIF(double TF)
{
    double dt = (1.0/((double) n_ld)) * TF;
    vector<vector<double>> S;
    vector<double> price(n_ld+1);
    vector<double> Payoff;
    vector<double> dW(n_ld+1);
    double tmp_option;
    double Discount = exp(-r*dt);//Discount factor

    //We simulate the paths
    double nb_itm_path = 0.0;
    nb_itm_path = simulateGBMPath(dt, &Payoff, &S);

    vector<double> tau (nb_itm_path), gamma (nb_itm_path);//optimal stopping time

    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [n_ld] += 1.0/(2.0* N) * Payoff[i];
        tau [i] = gamma [i] = TF;
    }

    //    ublas::vector <double> beta_boost(Order);
    Eigen::VectorXd beta_eigen(Order);
    for(int t = n_ld-1; t > 0; t--)//time
    {
        price [t] = 0.0;
        beta_eigen = beta_Laguerre_eigen_version(t, nb_itm_path, Order, Discount, &Payoff, &S);
        for(int i = 0; i < nb_itm_path; i++)//i = m simulations
        {
            tmp_option = option (S[i][t], K);
            double CE = 0.0; //CE = Conditional Expectation
            double Weight = exp(-0.5*S[i][t]);
            for (int j = 0; j <= Order; j++)//j = laguerre polynimial
            {
                CE += beta_eigen[j]*Discount*Weight*boost::math::laguerre(j, S[i][t]);
            }

            //Why CE < 0 sometimes ?
            //min(max(Option, E(...|F_t)),Option+Penalty)//Snell envelop
            if (CE <= tmp_option) //min(U,max(L,CE))=min(U,L)=L
            {
                Payoff [i] = tmp_option;//==g(S^i_t)
                tau [i] = t*dt;
            }
            else if (tmp_option < CE)//CE = max(L,CE)
            {
                if (CE < tmp_option + penalty)//min(U,CE)=CE
                {
                    Payoff [i] = Discount*Payoff [i];
                }
                //we are below the Lower barrier of the CR
                else //min(U,CE)=U
                {
                    Payoff [i] = tmp_option + penalty;
                    gamma [i] = t*dt;
                }
            }

            price [t] += (1.0/(2.0*N)) * Payoff[i];
        }
    }

    double tau_opt = 0.0, sigma_opt = 0.0;
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        price [0] += (1.0/(2.0*((double) N))) * Discount * Payoff[i];
        tau_opt += (1.0/(2.0*((double) N))) * tau [i];
        sigma_opt += (1.0/(2.0*((double) N))) * gamma [i];
    }

    cout << "disp('Price = " << price [0] << "')" << endl;
    cout << "disp('Average tau F = " << tau_opt << ", sigma F = " << sigma_opt << "')" << endl;
    return price;
}



//We simulate the paths
double simulateGBMPath(double dt, vector<double> * Payoff, vector<vector<double>> * S)
{
    vector<vector<double>> * dW;
    dW = simulateFBrownian (dt);
    int lenghtdW = (*dW).size();
    double nb_itm_paths = 0.0;

    for (int i = 0; i < lenghtdW; i++)//N == simultations
    {
        bool bool_itm_path = false;
        vector<double> X(n_ld+1);
        X[0] = s0;
        if (option(s0, K) > 0.001)
        {
            bool_itm_path = true;
            nb_itm_paths++;
        }
        for (int t = 1; t <= n_ld; t++)//t == time [1,m] W_t
        {
            X [t] = X [t-1]*exp( (r-0.5*sigma_square)*dt + sigma*(*dW)[i][t] );
            X [t] = X [t-1]*(1.0 + r*dt + sigma*(*dW)[i][t]);
            double opt = option(X[t], K);
            if(!bool_itm_path && option(X[t], K) > 0.001)
            {
                bool_itm_path = true;
                nb_itm_paths++;
            }
        }

        if(bool_itm_path)//path in the money
        {
            (*S).push_back(X);
            (*Payoff).push_back(option (X[n_ld], K));//==L_T, exp(-r*(T-T))=1.0
        }
//        else
//        {
//            cout << "not in the money" << endl;
//        }
    }

//    for (int i = 0; i < nb_itm_paths; i++)
//    {
//        bool test = true;
//        for (int t = 0; t <= m; t++)//t == time [1,m] W_t
//        {
//            if (option((*S)[i][t], K) > 0.5)
//            {
//                test = false;
//                break;
//            }
//        }
//        if (test)
//            cout << "problem" << endl;
//    }
//        free *dW;
//    nb_itm_paths = 2.0*N;
    return nb_itm_paths;
}

vector<vector<double>> * simulateFBrownian(double dt)
{
    vector<vector<double>> * dW = new vector<vector<double>> (2*N);
    double RV = 0.0;
    std::mt19937 gen(time(0));
    std::normal_distribution<double> dist(0.0, 1.0);

    for (int i = 0; i < 2*N; i++/*=2*/)//N == simultations
    {
        (*dW)[i].push_back(0.0);
//        (*dW)[i+1].push_back(0);
        for (int t = 1; t <= n_ld; t++)//t == time [1,m] W_t
        {
            RV = dist(gen);
            (*dW)[i].push_back(sqrt(dt)*RV);
//            (*dW)[i+1].push_back(sqrt(dt)*(-RV));//anthitetic variate
        }
    }

    return dW;
}

#endif // LSM_IF_H
