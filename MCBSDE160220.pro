TEMPLATE = app
CONFIG += console c++11
CONFIG += console c++11
CONFIG -= qt

QMAKE_CXXFLAGS += -DNDEBUG -DBOOST_UBLAS_NDEBUG

SOURCES += \
        main.cpp

HEADERS += \
    LSM.h \
    LSM_G_Fversion.h \
    LSM_IF.h \
    LSM_IG.h \
    gameoptionunderIF.h \
    gameoptionunderIG.h \
    invertMatrix.h \
    parameters.h \
    plot.h

win32 {
    INCLUDEPATH += E:/boost/boost_1_72_0
    INCLUDEPATH += E:/eigen
    INCLUDEPATH += C:/boost/boost_1_72_0
    INCLUDEPATH += C:/eigen
}

#-DNDEBUG -DBOOST_UBLAS_NDEBUG
