The question of enlargement of filtration goes back to the 80's. It had been studied on Brownian filtration, extended by a Random Variable (see **X**).

In this work **XX**, we want to know the price of an option as an insider position (who has more information). The stochastic filtration is different from the natural one (common players), and it is fed by a càdlàg process. The insider has new information at every milliseconds. How to deal with it? 
Hence, the challenge is big : The diffusion is non-markovian, the Brownian motion/Martingale property does not remain, and so the market may not be efficient. 
Furthermore, a Game option is an American derivative where the seller can stop at anytime. 
In this context if the insider is the seller, when will it stop the option ? What is the best time ? Is the game fair ? Do we have an hedging strategy ? Under which conditions ? (see my article on enlargement of filtration).

Here, I suggest a very new algorithm which is the result of a research challenge in order to do the simulations for pricing, notably under a non-markovian market, with no martingale and also integrate the new informations (Github coming soon).

**X** Jeulin, Yor - Grossissements de filtrations : exemples et applications, Séminaire de Calcul Stochastique 1982/83 Université Paris VI

**XX** Doubly reflected BSDE in an enlarged filtration via a process and connection with some applications, Gottesman Berdah article: 
https://scholar.google.com/citations?view_op=view_citation&hl=fr&user=WDIm_XcAAAAJ&citation_for_view=WDIm_XcAAAAJ:2osOgNQ5qMEC

Benjamin Gottesman Berdah.

Beni[d0t]gottesman[a.t.]gmail[d0t]com