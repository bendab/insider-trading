#ifndef PLOT_H
#define PLOT_H

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <vector>
#include <parameters.h>
#include <gameoptionunderIF.h>
#include <gameoptionunderIG.h>
#include <LSM_IF.h>
#include <LSM_IG.h>

void plot (double M, int ordinate);
void plot2d (vector<double> price, string variable);
void plotKvariable (double K_init, double K_final, int step);
void StoppingRegion (double tfinal, int time_Section, int SR_Section,
                     double ordinate_min, double ordinate_max,
                     double t0);

void plot2d (vector<double> price, string variable, string filtration)
{
    cout << variable << " = [";
    for (int t = 0; t <= n_ld-1; t++)
    {
        cout << price[t] << ", ";
    }
    cout << price[n_ld] << "];" << endl;
    cout << "plot(t," << variable << ",'DisplayName', 'Game value in " << filtration << "');"<< endl;
}


void StoppingRegion (double tfinal, int time_Section, int SR_Section,
                     double ordinate_min, double ordinate_max,
                     double t0)
{
    double time, value;
    bool frontier_down;
    double dt = M/n_ld;
    vector<double> epsilon;
    vector<vector<double>> * X = simulateExtraInformation(dt, &epsilon);
    vector<vector<double>> * W = simulateBM (dt);
    string SR_down, SR_up;
    SR_down += "SR_down = [";
    SR_up += "SR_up = [";
    for (int t = 0; t <= time_Section; t++)
    {
        time = t0 + t*(tfinal-t0)/time_Section;
        frontier_down = false;
        cout << "t = " << t << ", time section = " << time_Section << endl;
        for (int i = 0; i <= SR_Section; i++)
        {
            s0 = ordinate_min +
                    ((double) i)*(ordinate_max-ordinate_min)/SR_Section;
            value = LSMinIG_ForStoppingRegion(tfinal-time, W, X, false);
            if (fabs(value-option(s0,K)) < 0.001)
            {
                if (!frontier_down)
                {
                    SR_down += to_string(value);
                    if (t<time_Section)
                        SR_down += ",";
                    frontier_down = true;
                }
                else
                {
                    SR_up += to_string(value);
                    if (t<time_Section)
                        SR_up += ",";
                    break;
                }
            }
        }
    }
    SR_down += "];";
    SR_up += "];";
    cout << "t = 0:"<<tfinal<<"/"<<time_Section<<":"<< tfinal << ";" << endl;
    cout << "hold on"<< endl;
    cout << SR_down << endl;
    cout << SR_up
         << endl;
    //cout << "title('" << option << '2-D Line Plot')"<< endl;
    cout << "xlabel('Time');"<< endl;
    cout << "ylabel('Stopping Region');"<< endl;
    cout << "legend"<< endl;
}


void plot (double M, int m)
{
    cout << "t = 0:"<<M<<"/"<<m<<":"<< M << ";" << endl;

    cout << "hold on"<< endl;
    //cout << "title('" << option << '2-D Line Plot')"<< endl;
    cout << "xlabel('Time');"<< endl;
    cout << "ylabel('Game value');"<< endl;
    cout << "legend"<< endl;
}

void plotKvariable (double K_init, double K_final, int step)
{
    double tmp_K = K;
    double valueLSMG, valueLSMF;
    string sf, sg;

    sf += "sf = [";
    sg += "sg = [";
    for (K = K_init; K <= K_final; K=K+step)
    {
        valueLSMF = (LSMinIF(T))[0];
        sf += std::to_string(valueLSMF) + ", " ;
        valueLSMG = (LSMinIG_epsilonRVOld(T))[0];
        sg += std::to_string(valueLSMG) + ", ";
        cout << K << endl;
    }

    sf += "];";
    sg += "];";

    cout << "K = " << K_init <<":"<< step <<":"<< K_final << ";" << endl;

    cout << sf << endl;
    cout << sg << endl;
    cout << "hold on"<< endl;
    //cout << "title('" << option << '2-D Line Plot')"<< endl;
    cout << "xlabel('Strike K');"<< endl;
    cout << "ylabel('Initial Game value');"<< endl;
    cout << "legend"<< endl;
    cout << "plot(K,sf,'DisplayName', 'Game value in F');"<< endl;
    cout << "plot(K,sg,'DisplayName', 'Game value in G');"<< endl;
    K = tmp_K;
}
#endif // PLOT_H
