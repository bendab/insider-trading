#ifndef GAMEOPTIONUNDERIF_H
#define GAMEOPTIONUNDERIF_H
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <vector>
#include <parameters.h>

using namespace std;

float gameOptionPricingUnderIF_Naive (double T_IF);
float gameOptionPricingUnderIF (double T_IF);
float monte_carlo_call_price(const int& num_sims, const double& S, const double& K, const double& r, const double& v, const double& T);
float gameOptionPricingUnderIF_vectorversion (double T_IF);


float gameOptionPricingUnderIF_vectorversion (double T_IF)
{
    float U, L;
    float pn, E_pn_plus;
    float delta = (1.0/n_ld) * T_IF;
    vector<vector<float>> S(N);
    float DeltaM, tmp_vol;
    float tmp, dW, tau = 0.0, gamma = 0.0;
    float sup_tau = 0.0f, inf_gamma = 0.0f, sup_y = 0.0f, inf_y = 0.0f;

    //We simulate the paths
    for (int i = 0; i < N; i++)
    {
        S[i] = vector <float> (n_ld+1);
        S[i][0] = s0;
        for (int j = 1; j <= n_ld; j++)
        {
            dW = sqrt(delta)*gaussian_box_muller();
            S [i][j] = S [i][j-1]*(1.0f + sigma*dW + mu*delta);
            //S [i][j] = S [i][j-1] * exp(sigma*sqrt(delta)*gaussian_box_muller() - 0.5f*sigma_square*delta);
        }
    }

    //Initialization
    pn = 0.0;
    float tmp_S, x;
    for(int i = 0; i < N; i++)
    {
        //E_pn_plus += (1.0f/N) * option (S [i][m], K);
        tmp_S = exp(sigma*sqrt(delta)*gaussian_box_muller())*exp(-sigma_square/2.0*delta);
        x = exp(sigma*sqrt((n_ld-1)*delta)*gaussian_box_muller());
        E_pn_plus += (1.0f/N) * option (x*tmp_S, K);
    }

    sup_tau = inf_gamma = n_ld*delta;
    sup_y = inf_y = E_pn_plus;

    //induction
    for(int j = n_ld-1; j >= 1; j--)//time
    {
        tmp = 0.0;
        for (int i = 0; i < N; i++)//number of simulation
        {
            tmp_S = exp(sigma*sqrt(delta)*gaussian_box_muller() - sigma_square/2.0*delta);
            x = exp(sigma*sqrt((j-1)*delta)*gaussian_box_muller());
            //L = option (S[i][j], K)/S[i][j-1];
            L = option (x*tmp_S, K);
            U = L + penalty;
            pn = max (L, E_pn_plus);
            //pn = min (U, max (L, E_pn_plus));
            tmp += (1.0/N) * pn;
            if (equal(L,pn))
                tau += delta*j/(N*N);
            else if (equal(U, pn))
                gamma += delta*j/(N*N);
        }
        E_pn_plus = tmp;
        //if (sup_y <= E_pn_plus)
        if (sup_y-E_pn_plus <= 0.0001f)
        {
            sup_y = E_pn_plus;
            sup_tau = j*delta;
        }
        if (E_pn_plus-inf_y <= 0.0001f)
        {
            inf_y = E_pn_plus;
            inf_gamma = j*delta;
        }
    }

//    cout << "tau F = " << tau << ", gamma F = " << gamma <<endl;
    cout << "sup_tau F = " << sup_tau << ", inf_gamma F = " << inf_gamma <<endl;

    return E_pn_plus;
}

float gameOptionPricingUnderIF (double T_IF)
{
    float S_t, Kp, Km, U, L;
    float y, E_y_plus;
    float tmp_W, t, g;
    float tmpY = 0.0;
    float delta = (T_IF/n_ld);
    float expected_value = 0.0;
//    float penalization_cst = (penalization*delta)/(1.0+penalization*delta);

    //initialization
    for(int i = 0; i < N; i++)
    {
        tmp_W = (sqrt(T_IF))*gaussian_box_muller();
        S_t = s0 * exp(sigma*tmp_W + (mu-0.5*sigma_square)*T_IF);
        tmpY += 1.0f/N * option(S_t, K);
    }

    for(int j = n_ld-1; j >= 0; j--)
    {
        E_y_plus = tmpY;
        tmpY = 0.0;
        t = delta * j;
        for(int i = 0; i < N; i++)
        {
            tmp_W = (sqrt(t)*delta)*gaussian_box_muller();
            S_t = s0 * exp(sigma*tmp_W + (mu-0.5f*sigma_square)*t);
            L = option (S_t, K);
            U = L + penalty;
            y = min (U, max (L, E_y_plus));
            tmpY += (1.0f/N) * y;
        }
    }

    expected_value = tmpY;
    return expected_value;
}

// Pricing a European vanilla call option with a Monte Carlo method
float monte_carlo_call_price(const int& num_sims, const double& S, const double& K, const double& r, const double& v, const double& T) {
    double S_adjust = S * exp(T*(r-0.5*v*v));
    double S_cur = 0.0;
    double payoff_sum = 0.0;

    for (int i=0; i<num_sims; i++) {
        double gauss_bm = gaussian_box_muller();
        S_cur = S_adjust * exp(sqrt(v*v*T)*gauss_bm);
        payoff_sum += option (S_cur, K);
    }

    return (payoff_sum / static_cast<double>(num_sims)) * exp(-r*T);
}

float gameOptionPricingUnderIF_Naive (double T_IF)
{
    float W_t[n_ld+1];
    float S_t, dW_t, Kp, Km, U, L, Z;
    float y, y_plus;
    float tmp_W, t, g;
    float delta = (T_IF/((double) n_ld));
    float expected_value = 0.0;
//    float penalization_cst = (penalization*delta)/(1.0+penalization*delta);

    for (int i = 0; i<=N; i++)
    {
        W_t [0] = 0.0;
        for(int k = 1; k <= n_ld; k++)
        {
            t = delta * k;
            tmp_W = (sqrt(delta))*gaussian_box_muller();
            W_t [k] = W_t [k-1] + tmp_W;
        }

        dW_t = W_t [n_ld] - W_t [n_ld-1];
        S_t = s0 * exp(sigma*W_t [n_ld] + (mu-0.5*sigma_square)*T_IF);
        y = max (0.0, S_t - K);
        for(int j = n_ld-1; j >= 0; j--)
        {
            t = delta * j;
            y_plus = y;
            dW_t = W_t [j+1] - W_t [j];

            S_t = s0 * exp(sigma*W_t [j] + (mu-0.5*sigma_square)*t);
            Z = 1.0/N * (1.0/sqrt(delta) * (y_plus*dW_t));
            g = S_t*Z*mu;// = 0
            {
                L = max (0.0, S_t - K);
                U = L + penalty;
                Kp = 0.0;//penalization_cst * (max (0.0f, L - (y_plus + g)));
                Km = 0.0f;//penalization_cst * (max (0.0f, (y_plus + g)-U));
            }
            y = y_plus + g*delta - Z*sqrt(delta)*dW_t + (Kp-Km);
        }

        expected_value += (1.0/N) * y;
    }

    return expected_value;
}




#endif // GAMEOPTIONUNDERIF_H
