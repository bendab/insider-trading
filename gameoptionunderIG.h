#ifndef GAMEOPTIONUNDERIG_H
#define GAMEOPTIONUNDERIG_H

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <vector>
#include <parameters.h>

float gameOptionPricingUnderIG_vectorversion (double T_IG);
float gameOptionPricingUnderIG_new (double T_IG);
float gameOptionPricingUnderIG (double T_IG);
float gameOptionPricingUnderIG_Fpayoff_vectorversion (double T_IG);

float gameOptionPricingUnderIG (double T_IG)
{
    float tmpM, tmpX, tmpW, tmpY;
    float Ct_tmp;
    float S_t, U, L;
    float y, E_y_plus;
    float t;
    float delta = (1.0/n_ld) * T_IG;
    float expected_value = 0.0;

    Ct_tmp = Ct(T_IG);
    tmpY = 0.0;
    for(int i = 0; i < N; i++)
    {
        tmpM = (sqrt(T_IG))*gaussian_box_muller();
        tmpX = epsilon_ext*(sqrt(T_IG))*gaussian_box_muller() + x0;
        //        float W1 = (sqrt(T))*gaussian_box_muller();
        //        tmpX = W1 + epsilon*(sqrt(T-T_IG))*gaussian_box_muller();
        tmpW = (1.0/(1.0-Ct_tmp)) * (tmpM - Ct_tmp*tmpX);
        S_t = s0 * exp (sigma*tmpW - 0.5f*sigma_square*T_IG);
        tmpY += (1.0f/N) * option (S_t,K);
    }

    for(int j = n_ld-1; j >= 0; j--)
    {
        E_y_plus = tmpY;
        tmpY = 0.0f;
        t = delta * j;
        Ct_tmp = Ct(t);
        for (int i = 0; i<N; i++)
        {
            tmpM = (sqrt(t))*gaussian_box_muller();
            tmpX = epsilon_ext*(sqrt(t))*gaussian_box_muller() + x0;
            //            float W1 = (sqrt(T))*gaussian_box_muller();
            //            tmpX = W1 + epsilon*(sqrt(T-t))*gaussian_box_muller();
            tmpW = (1.0/(1.0-Ct_tmp)) * (tmpM - Ct_tmp*tmpX);
            S_t = s0 * exp (sigma*tmpW - 0.5f*sigma_square*t);

            L = max (0.0, S_t - K);
            U = L + penalty;
            y = min (U, max (L, E_y_plus));
            tmpY += (1.0/N) * y;
        }
    }

    expected_value = tmpY;
    return expected_value;
}

float gameOptionPricingUnderIG_Fpayoff_vectorversion (double T_IG)
{
    float Ct_tmp;
    float U, L;
    float pn, E_pn_plus;
    float delta = (1.0/n_ld) * T_IG;
    float expected_value = 0.0;
    vector<vector<float>> X(N), SumX(N), S(N), W(N), P_in(N);
    float DeltaW, DeltaM, DeltaX, tmp_vol;
    float dW, dX;
    float h_X;
    float tmp, tau = 0.0f, gamma = 0.0f;
    float sup_tau = 0.0f, inf_gamma = 0.0f, sup_y = 0.0f, inf_y = 0.0f;

    //We simulate the paths
    for (int i = 0; i < N; i++)
    {
        SumX[i] = vector <float> (n_ld+1), P_in[i] = vector <float> (n_ld+1);
        X[i] = vector <float> (n_ld+1), S[i] = vector <float> (n_ld+1);
        W[i] = vector <float> (n_ld+1);
        SumX[i][0] = x0, X[i][0] = x0, S[i][0] = s0;
        P_in[i][0] = 1.0f, W[i][0] = 0.0f ;
        for (int j = 1; j <= n_ld; j++)
        {
            X [i][j] = X [i][j-1] + epsilon_ext*sqrt(delta)*gaussian_box_muller();
            W [i][j] = W [i][j-1] + sqrt(delta)*gaussian_box_muller();
            dX = X [i][j]-X [i][j-1];
            SumX [i][j] += X [i][j];
            //tmp = normal_pdf (-dX/(epsilon*sqrt(delta)), 0.0f, 1.0f);
            tmp = (1.0f/sqrt(delta))*normal_pdf (-dX/(delta), 0.0f, 1.0f);
            P_in[i][j] = P_in[i][j-1]*tmp;
            dX = X [i][j] - X [i][j-1];
            dW = W [i][j] - W [i][j-1];//sqrt(delta)*gaussian_box_muller();
            S [i][j] = S [i][j-1] * (1.0f + sigma*(dW) + mu*delta);
        }
    }

    //Initialization
    E_pn_plus = 0.0;
    float w_t, t, tmp_pin;
    t = n_ld*delta;
    for(int i = 0; i < N; i++)
    {
        //w_t = sqrt(m)*(S [i][m]/S [i][m-1] - 1.0f - mu*delta)/(sigma);
        w_t = W[i][n_ld];
        tmp_pin *= P_in [i][n_ld-1]*normal_pdf (((X[i][n_ld]/*-x0*/)-w_t)/(sqrt(1.0f-t+epsilon_square_ext*(1.0f-t))), 0.0f, 1.0f);
        //if (p_in(m, SumX[i][m], &(X[i]), w_t, t, t, delta) <= 0.0001f)
        if(tmp_pin <= 0.0001f)
        {
            E_pn_plus += 0.0f;
        }
        else
            E_pn_plus += (1.0f/N) * option (S [i][n_ld], K);
    }

    sup_tau = inf_gamma = t;
    sup_y = inf_y = E_pn_plus;
    //induction
    for(int j = n_ld-1; j >= 1; j--)//time
    {
        tmp = 0.0;
        t = j*delta;
        for (int i = 0; i < N; i++)//number of simulation
        {
            //w_t = sqrt(j)*(S [i][j]/S [i][j-1] - 1.0f - mu*delta)/(sigma);
            w_t = W[i][j];
            tmp_pin = P_in [i][j-1]*normal_pdf ((SumX[i][j]-w_t)/(sqrt(T-t+epsilon_square_ext*(T-t))), 0.0f, 1.0f);
            //if (p_in(j, SumX[i][j], &(X[i]), w_t, t, t, delta) <= 0.0001f)
            if (tmp_pin <= 0.0001f)
            {
                tmp += 0.0f;
            }
            else
            {
                L = option (S[i][j], K)/(S[i][j-1]);
                U = L + penalty;
                pn = min (U, max (L, E_pn_plus));
                tmp += (1.0/N) * pn;
                if (equal(L,pn))
                    tau += delta*j/(N*N);
                else if (equal(U, pn))
                    gamma += delta*j/(N*N);
            }
        }
        E_pn_plus = tmp;
        if (sup_y-E_pn_plus <= 0.0001f)
        {
            sup_y = E_pn_plus;
            sup_tau = t;
        }
        if (E_pn_plus-inf_y <= 0.0001f)
        {
            inf_y = E_pn_plus;
            inf_gamma = t;
        }
    }

//    cout << "tau G = " << tau << ", gamma G = " << gamma <<endl;
    cout << "sup_tau G = " << sup_tau << ", inf_gamma G = " << inf_gamma <<endl;

    return E_pn_plus;
}



float gameOptionPricingUnderIG_vectorversion (double T_IG)
{
    float Ct_tmp;
    float U, L;
    float pn, E_pn_plus;
    float delta = (1.0/n_ld) * T_IG;
    float expected_value = 0.0;
    vector<vector<float>> X(N), M(N), S(N), W(N);
    float DeltaW, DeltaM, DeltaX, tmp_vol;
    float tmp, tau = 0.0, gamma = 0.0;

    //We simulate the paths
    for (int i = 0; i < N; i++)
    {
        X[i] = vector <float> (n_ld+1), M[i] = vector <float> (n_ld+1);
        S[i] = vector <float> (n_ld+1), W[i] = vector <float> (n_ld+1);
        X[i][0] = x0, W [i][0] = w0, M[i][0] = 0.0f;
        S[i][0] = s0;
        for (int j = 1; j <= n_ld; j++)//time
        {
            Ct_tmp = Ct(delta*j);
            //            cout << "t = " << delta*j << ", i = " << i << ", j = " << j << ", ct = " << Ct_tmp << endl;
            X [i][j] = X [i][j-1] + epsilon_ext*sqrt(delta)*gaussian_box_muller();
            M [i][j] = M [i][j-1] + sqrt(delta)*gaussian_box_muller();
            DeltaM = M [i][j] - M [i][j-1];
            //DeltaX = X [i][j] - X [i][j-1];
            //tmp_vol = volatility(X[i][j]);
            //S [i][j] = S [i][j-1] * exp (sigma*DeltaW - 0.5f*sigma_square*delta);
            //S [i][j] = S [i][j-1] * exp (sigma*DeltaM + (Ct_tmp*(W[i][j]-X[i][j]) - 0.5f*sigma_square)*delta);

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
            //Probleme quand on change le +/- un coup le call fonctionne et le put non et vice versa
            S [i][j] = S [i][j-1] * (1.0f + sigma*(DeltaM + Ct_tmp*(W[i][j-1]-X[i][j-1])*delta) + mu*delta);
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

            //            W [i][j] = (1.0/(1.0-Ct_tmp)) * (M [i][j] - Ct_tmp*X [i][j]);
            //            DeltaW = W [i][j] - W [i][j-1];
            //            S [i][j] = S [i][j-1] *(1.0f + sigma*DeltaW + mu*delta);
        }
    }

    //Initialization
    E_pn_plus = 0.0;
    for(int i = 0; i < N; i++)
    {
        E_pn_plus += (1.0f/N) * option (S [i][n_ld], K);
    }

    //induction
    for(int j = n_ld-1; j >= 1; j--)//time
    {
        tmp = 0.0;
        for (int i = 0; i < N; i++)//number of simulation
        {
            L = option (S[i][j], K)/S[i][j-1];
            U = L + penalty;
            pn = min (U, max (L, E_pn_plus));
            tmp += (1.0/N) * pn;
            if (equal(L,pn))
                tau += delta*j/N;
            else if (equal(U, pn))
                gamma += delta*j/N;
        }
        E_pn_plus = tmp;
    }

//    cout << "tau G = " << tau << ", gamma G = " << gamma <<endl;

    return E_pn_plus;
}




float gameOptionPricingUnderIG_new (double T_IG)
{
    float tmpM, tmpX, tmpW, tmpY;
    float Ct_tmp;
    float S_t, U, L;
    float y, E_y_plus;
    float t;
    float delta = (1.0/n_ld) * T_IG;
    float expected_value = 0.0;

    Ct_tmp = Ct(T_IG);
    tmpY = 0.0;
    for(int i = 0; i < N; i++)
    {
        tmpW = (sqrt(T_IG))*gaussian_box_muller();
        tmpX = epsilon_ext*(sqrt(T_IG))*gaussian_box_muller() + x0;
        S_t = s0 * exp (sigma*tmpW - 0.5f*sigma_square*T_IG);
        tmpY += (1.0f/N) * option (S_t,K) * (1.0/normalCDF(tmpX/epsilon_ext));
    }

    for(int j = n_ld-1; j >= 0; j--)
    {
        E_y_plus = tmpY;
        tmpY = 0.0f;
        t = delta * j;
        Ct_tmp = Ct(t);
        for (int i = 0; i<N; i++)
        {
            tmpW = (sqrt(t))*gaussian_box_muller();
            tmpX = epsilon_ext*(sqrt(t))*gaussian_box_muller() + x0;
            S_t = s0 * exp (sigma*tmpW - 0.5f*sigma_square*t);

            L = max (0.0, S_t - K);
            U = L + penalty;
            y = min (U, max (L, E_y_plus));
            tmpY += (1.0/N) * y * (1.0/normalCDF(tmpX/epsilon_ext));
        }
    }

    expected_value = tmpY;
    return expected_value;
}





#endif // GAMEOPTIONUNDERIG_H
