#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <vector>
#include <time.h>
#include <random>

using namespace std;

//Time Variables & Constants
double T = 2.0; //Information reveal Maturity
double M = 1.9;
const int n_ld = 10; //Time subdivision
const double N = 10000; //Numbers of simulations
const int Order = 10;//Laguerre Polynomial Order
const double timestep = T/n_ld;
//Time Variables & Constants

//BS model
double sigma = 40.0/100.0;//vol
double sigma_low = 20.0/100.0;//vol
double sigma_high = 80.0/100.0;//vol
double sigma_square = sigma*sigma;
double mu = 0.0/100.0;
double s0 = 90.0;
double r = 6.0/100.0;
//double S_t = s0;
//BS model

//Game Option Parameters
double K = 100.0; //Strike
double penalty = 12.0;
//double penalization = 20.0; //penalization scheme
//double PS_Cste = penalization*timestep/(1.0+penalization*timestep); //PS = penalization scheme
//Game Option Parameters

//Enlargement Parameters
double epsilon_ext = 0.5;
double epsilon_square_ext = epsilon_ext*epsilon_ext;
//Enlargement Parameters

//G_0 initial conditions
double w0 = 0.0, x0 = 0.0;

inline double option (double S, double K);
inline double call (double S, double K);
inline double put (double S, double K);
double gaussian_box_muller();
inline double volatility (double X);
inline double jump_volatility (double X);
inline double normal_pdf(double x, double n_ld, double s);
inline double normalCDF (double value);

inline double option (double S, double K)
{
    return put (S,K);
}

inline double call (double S, double K)
{
    return max (0.0, S-K);
}


inline double put (double S, double K)
{
    return max (0.0, K-S);
}

double normalCDF (double value)
{
    return 0.5 * erfc(-value * M_SQRT1_2);
}

inline bool equal(double a, double b)
{
    return fabs(a - b) < 0.1f;
}


inline double normal_pdf(double x, double m, double s)
{
    static const double inv_sqrt_2pi = 0.3989422804014327;
    double a = (x - m) / s;

    return inv_sqrt_2pi / s * std::exp(-0.5f * a * a);
}

inline double p_in (int m, double SumX, vector<double> * X, double wt, double t, double ti, double delta)
{
    vector<double> &x = *X;
    double sum_x = 0.0f;
    double prod = 1.0;

    //    for (int j = 0; j < i; j++)
    //    {
    //        sum_x += x[j];
    //    }

    for (int j = 0; j < m-1; j++)
    {
        prod *= 1.0f/sqrt(delta) * normal_pdf (-x[j]/(delta), 0.0f, 1.0f);
    }

    return prod * normal_pdf ((SumX-wt)/(sqrt(1.0f-t+epsilon_square_ext*(1.0f-ti))), 0.0f, 1.0f);
}

inline double volatility (double X)
{
    return jump_volatility (X);
}


inline double jump_volatility (double X)
{
    if (X>1.0 || X < -1.0)
        return sigma_high;
    return sigma_low;
}

inline double Ct (double time)
{
    return  1.0/( (T-time)*(1.0+epsilon_square_ext) );
}

double gaussian_box_muller()
{
    double x = 0.0;
    double y = 0.0;
    double euclid_sq = 0.0;

    // Continue generating two uniform random variables
    // until the square of their "euclidean distance"
    // is less than unity
    do {
        x = 2.0 * rand() / static_cast<double>(RAND_MAX)-1;
        y = 2.0 * rand() / static_cast<double>(RAND_MAX)-1;
        euclid_sq = x*x + y*y;
    } while (euclid_sq >= 1.0);

    return x*sqrt(-2*log(euclid_sq)/euclid_sq);
}



#endif // PARAMETERS_H
