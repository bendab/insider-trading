#ifndef LSM_H
#define LSM_H
#include <iostream>
#include <cmath>
#include <algorithm>
#include <parameters.h>
///////BOOST LIBRARIES//////
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/math/special_functions/laguerre.hpp>
#include <invertMatrix.h>
#include <eigen/Dense>
namespace ublas = boost::numeric::ublas;
///////BOOST LIBRARIES//////

#include <vector>
using namespace std;

inline double Laguerre_Polynomial (int order, double x);
ublas::vector <double> beta_Laguerre (int t, int nb_itm_path, int Order, double delta,
                    vector<double> *Payoff, vector<vector<double> > *S);

vector<double> ConditionalExpectation (int t, int nb_itm_path, int Order, double delta,
                                      vector<double> *Payoff, vector<vector<double> > *S);
ublas::vector <double> beta_Laguerre (int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S, vector<vector<double>> * p);

vector<vector<double>> * simulateBM(double dt);
vector<vector<double>> * simulateExtraInformation(double dt, vector<double> *epsilon_ext);
Eigen::VectorXd beta_Laguerre_eigen_version (int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S);
Eigen::VectorXd beta_Laguerre_eigen_version(int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S, vector<vector<double>> * p);
//We compute the beta's


ublas::vector <double> beta_Laguerre (int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S, vector<vector<double>> * p)
{
    int tmpOrder = Order+1;
    ublas::matrix <double> L(nb_itm_path,tmpOrder), Lt(tmpOrder,nb_itm_path);//order+1 because 0,1..order, i=1..N
    ublas::matrix <double> LtL(tmpOrder,tmpOrder), invLtL(tmpOrder,tmpOrder);
    ublas::vector <double> Lty(nb_itm_path), Lty_up(nb_itm_path);
    ublas::vector <double> beta(tmpOrder), P(tmpOrder), y(nb_itm_path);
    bool matrix_inverted;

    //We build L
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        for (int j = 0; j <= Order; j++)//j = laguerre polynimial
        {
            L(i,j) = boost::math::laguerre(j, (*S)[i][t]);//Laguerre_Polynomial (S[i][t], j);
        }
        y[i] = Discount * (*Payoff) [i]* (*p) [i][t];
    }

    ///(Lt*L)^-1*Lt*y////
    Lt = boost::numeric::ublas::trans(L);//L^t
    LtL = prod(Lt,L);//Lt*L
    matrix_inverted = InvertMatrix (LtL, invLtL);//(Lt*L)^-1
    if (!matrix_inverted)
    {
        cout << "error time = " << t << endl;
    }
    Lty = prod(Lt, y);//Lt*y
    beta = prod (invLtL, Lty);//(Lt*L)^-1*Lt*y
    ////(Lt*L)^-1*Lt*y////

    return beta;
}

ublas::vector <double> beta_Laguerre (int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S)
{
    int tmpOrder = Order+1;
    ublas::matrix <double> L(nb_itm_path,tmpOrder), Lt(tmpOrder,nb_itm_path);//order+1 because 0,1..order, i=1..N
    ublas::matrix <double> LtL(tmpOrder,tmpOrder), invLtL(tmpOrder,tmpOrder);
    ublas::vector <double> Lty(nb_itm_path), Lty_up(nb_itm_path);
    ublas::vector <double> beta(tmpOrder), P(tmpOrder), y(nb_itm_path);
    bool matrix_inverted;

    //We build L
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        for (int j = 0; j <= Order; j++)//j = laguerre polynimial
        {
            L(i,j) = boost::math::laguerre(j, (*S)[i][t]);//Laguerre_Polynomial (S[i][t], j);
        }
        y[i] = Discount * (*Payoff) [i];
    }

    ///(Lt*L)^-1*Lt*y////
    Lt = boost::numeric::ublas::trans(L);//L^t
    LtL = prod(Lt,L);//Lt*L
    matrix_inverted = InvertMatrix (LtL, invLtL);//(Lt*L)^-1
    if (!matrix_inverted)
    {
        cout << "error time = " << t << endl;
    }
    Lty = prod(Lt, y);//Lt*y
    beta = prod (invLtL, Lty);//(Lt*L)^-1*Lt*y
//    std::cout << "beta = "<< beta << std::endl;
    ////(Lt*L)^-1*Lt*y////

    return beta;
}


Eigen::VectorXd beta_Laguerre_eigen_version (int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S, vector<vector<double>> * p)
{
    int tmpOrder = Order+1;
    Eigen::MatrixXd L(nb_itm_path,tmpOrder);//order+1 because 0,1..order, i=1..N
    Eigen::VectorXd beta(tmpOrder), y(nb_itm_path);

    //We build L
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        double Weight = exp(-0.5*(*S)[i][t]);
        for (int j = 0; j <= Order; j++)//j = laguerre polynimial
        {
            L(i,j) = Weight * boost::math::laguerre(j, (*S)[i][t]);
        }
        y(i) = (*Payoff) [i]/** (*p) [i][t]*/;
    }

    ///(Lt*L)^-1*Lt*y////
    beta = (1.0/(2.0*N) * (L.transpose() * L)).ldlt().solve(1.0/(2.0*N) * (L.transpose() * y));
    ////(Lt*L)^-1*Lt*y////

    return beta;
}


Eigen::VectorXd beta_Laguerre_eigen_version(int t, int nb_itm_path, int Order, double Discount,
                    vector<double> * Payoff, vector<vector<double>> * S)
{
    int tmpOrder = Order+1;
    Eigen::MatrixXd L(nb_itm_path,tmpOrder);//order+1 because 0,1..order, i=1..N
    Eigen::VectorXd beta(tmpOrder), y(nb_itm_path);

    //We build L
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        double Weight = exp(-0.5*(*S)[i][t]);
        for (int j = 0; j <= Order; j++)//j = laguerre polynimial
        {
            L(i,j) = Weight * boost::math::laguerre(j, (*S)[i][t]);
        }
        y[i] = (*Payoff) [i];
    }

    ///(Lt*L)^-1*Lt*y////
    //beta = (L.transpose() * L).ldlt().solve(L.transpose() * y);
    beta = (1.0/(2.0*N) * (L.transpose() * L)).ldlt().solve(1.0/(2.0*N) * (L.transpose() * y));
    ////(Lt*L)^-1*Lt*y////

    return beta;
}

//We simulate the BM paths
vector<vector<double>> * simulateBM(double dt)
{
    vector<vector<double>> * W = new vector<vector<double>> (2*N);
    double RV = 0.0;
    std::mt19937 e2(time(0));
    std::normal_distribution<double> dist(0.0, 1.0);

    for (int i = 0; i < 2*N; i+=2)//N == simultations
    {
        (*W)[i].push_back(w0);
        (*W)[i+1].push_back(w0);
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            RV = dist(e2);
            (*W)[i].push_back((*W)[i][t-1] + sqrt(dt)*RV);
            (*W)[i+1].push_back((*W)[i+1][t-1] + sqrt(dt)*(-RV));//anthitetic variate
        }
    }

    return W;
}

vector<vector<double>> * simulateExtraInformation(double dt, vector<double> * epsilon)
{
    vector<vector<double>> * X = new vector<vector<double>> (2*N);
    std::mt19937 e2(time(0));
    std::normal_distribution<double> dist(0.0, 1.0);
    double RV = 0.0;

    for (int i = 0; i < 2*N; i+=2)//N == simultations
    {
        (*epsilon).push_back(dist(e2));//Random variable
        (*epsilon).push_back(dist(e2));
        (*X)[i].push_back(x0);
        (*X)[i+1].push_back(x0);
        for (int t = 1; t <= n_ld; t++)//t == time
        {
            RV = dist(e2);
            (*X)[i].push_back((*X)[i][t-1] + (*epsilon)[i]*sqrt(dt)*RV);
            (*X)[i+1].push_back((*X)[i+1][t-1] + (*epsilon)[i+1]*sqrt(dt)*(-RV));//anthitetic variate
        }
    }

    return X;
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

vector<double> ConditionalExpectation (int t, int nb_itm_path, int Order, double delta,
                                      vector<double> * Payoff, vector<vector<double>> * S)
{
    ublas::vector <double> beta(Order+1), beta_up (Order+1);
    vector <double> CE(nb_itm_path);
    beta = beta_Laguerre(t, nb_itm_path, Order, delta, Payoff, S);
    for(int i = 0; i < nb_itm_path; i++)//i = m simulations
    {
        double tmp_option = option ((*S)[i][t], K);
        CE [i] = 0.0f;
        for (int j = 0; j <= Order; j++)//j = laguerre polynimial
        {
            //CE = Conditional Expectation
            CE [i] += beta[j]*boost::math::laguerre(j, (*S)[i][t]);
//                CE [i] += beta[j]*Laguerre_Polynomial(S[i][t], j);
//            CE_up [i] += CE [i]+penalty;//beta_up[j]*Lagrange_Polynomial(S[i][t], j);
        }
    }
    return CE;
}

#endif // LSM_H
